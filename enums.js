const countryCodeEnum = {
    UK: 'UK',
    GERMANY: 'Germany'
}

const productTypeEnum = {
    MASK: 'mask',
    GLOVES: 'gloves'
}
const OutputMsgEnum = {
    OUT_OF_STOCK: 'OUT_OF_STOCK',
    SUCCESS: ''
}
module.exports = {
    countryCodeEnum,
    productTypeEnum,
    OutputMsgEnum
    
}