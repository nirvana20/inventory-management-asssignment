const { productInInventoryModel } = require('./model');
const { productTypeEnum, OutputMsgEnum, countryCodeEnum} =   require('./enums');


function formattingInput(txt) {
    const inputLst = txt.split(':');
    let model = {
        country: null,
        passport: null,
        gloves: 0,
        mask: 0
    }
    if (inputLst.length == 6) {
        model.country = inputLst[0];
        model.passport = inputLst[1];
        if (inputLst[2] == 'Gloves') model.gloves = inputLst[3];
        else if (inputLst[4] == 'Gloves') model.gloves = inputLst[5];
        if (inputLst[2] == 'Mask') model.mask = inputLst[3];
        else if (inputLst[4] == 'Mask') model.mask = inputLst[5];
    }
    else if (inputLst.length == 5) {
        model.country = inputLst[0];
        if (inputLst[1] == 'Gloves') model.gloves = inputLst[2];
        else if (inputLst[3] == 'Gloves') model.gloves = inputLst[4];
        if (inputLst[1] == 'Mask') model.mask = inputLst[2];
        else if (inputLst[3] == 'Mask') model.mask = inputLst[4];
    }
    return model;
}

function checkIfOrderCanBePlaced(model) {
    const { gloves, mask } = model;
    const totalGlovesInInventory = productInInventoryModel.UK.gloves.quantity + productInInventoryModel.Germany.gloves.quantity;
    const totalMaskInInventory = productInInventoryModel.UK.mask.quantity + productInInventoryModel.Germany.mask.quantity < mask;

    if ( totalGlovesInInventory < gloves || totalMaskInInventory) return false;
    return true;
}

function checkAndFindPassportCountry(passport) {
    if (!passport) return null;
    if (passport.startsWith('A')) return countryCodeEnum.GERMANY;
    else if (passport.startsWith('B')) return countryCodeEnum.UK;
    return null;
}

function checkAndCalculateItemOverlimitFromOneInventory(model) {
    const { currentCountryDetails, minPriceFromOtherCountry, productType, quantity } = model

    let leftItems = 0;
    let leftAmount = 0;
    let totalPrice = 0;

    let returnModel = {
        productType: productType,
        totalPrice: 0,
        itemsRemaining: {
            [countryCodeEnum.UK]: 0,
            [countryCodeEnum.GERMANY]: 0
        },
        isApplied: false
    }
    if (minPriceFromOtherCountry.perPiecePrice < currentCountryDetails.perPiecePrice) {

        if (minPriceFromOtherCountry.quantity < quantity) {
            returnModel.isApplied = true;
            leftItems = quantity - minPriceFromOtherCountry.quantity;
            leftAmount = leftItems * productInInventoryModel[currentCountryDetails.country][productType].price;
            returnModel.totalPrice = minPriceFromOtherCountry.quantity * minPriceFromOtherCountry.perPiecePrice + leftAmount;
            returnModelgloves[currentCountryDetails.country] = productInInventoryModel[currentCountryDetails.country][productType].quantity - leftItems;
            returnModel[itemsRemaining][minGlovesPriceFromOtherCountry.countryName] = 0;
        }
    } else if (quantity > productInInventoryModel[currentCountryDetails.country].gloves.quantity) {
        returnModel.isApplied = true;
        leftItems = quantity - productInInventoryModel[currentCountryDetails.country][productType].quantity;
        leftAmount = leftItems * minPriceFromOtherCountry.perPiecePrice + Math.ceil(leftItems / 10) * minPriceFromOtherCountry.shipmentPricePer10Unit;
        returnModel.totalPrice = productInInventoryModel[currentCountryDetails.country][productType].quantity * currentCountryDetails.perPiecePrice + leftAmount;
        returnModel[currentCountryDetails.country] = 0;
        returnModel.itemsRemaining[minPriceFromOtherCountry.countryName] = productInInventoryModel[minPriceFromOtherCountry.countryName][productType].quantity - leftItems;

    }
    return returnModel;
}
function checkMinProductPrice(model) {

    const { ignoreCountry, discountCountry, productType } = model;
    let minPrice = Number.MAX_SAFE_INTEGER;
    let returnModel = {
        perPiecePrice: 0,
        shipmentPricePer10Unit: 400,
        countryName: null
    };
    Object.keys(productInInventoryModel).forEach((countryName) => {
        let inventoryDetails = productInInventoryModel[countryName];
        let priceDetails = {
            perPiecePriceAfterShipment: 0,
            perPiecePrice: inventoryDetails[productType].price,
            shipmentPricePer10Unit: 400,
            countryName: countryName,
            quantity: inventoryDetails[productType].quantity
        };
        if (countryName != ignoreCountry && discountCountry && countryName == discountCountry) {
            priceDetails.perPiecePriceAfterShipment = inventoryDetails[productType].price + 32;
            priceDetails.shipmentPricePer10Unit = 400 - (400 * 20) / 100;
        } else if (countryName != ignoreCountry) {
            priceDetails.perPiecePriceAfterShipment = inventoryDetails[productType].price + 40;
        }
        if (countryName != ignoreCountry && priceDetails.perPiecePriceAfterShipment < minPrice) {
            minPrice = priceDetails.perPiecePriceAfterShipment;
            returnModel = priceDetails;
        }
    });
    return returnModel;
}

function minimizeSalePrice(model) {
    let returnModel = {
        totalPrice: 0,
        mask: {
            UK: 100,
            Germany: 100
        },
        gloves: {
            UK: 100,
            Germany: 50
        },
        message: null
    }
    if (!checkIfOrderCanBePlaced(model)) {
        returnModel.message = OutputMsgEnum.OUT_OF_STOCK;
    } else {
        returnModel.message = OutputMsgEnum.SUCCESS;
        const { country, passport, gloves, mask } = model;
        const minGlovesPriceFromOtherCountry = checkMinProductPrice({ ignoreCountry: country, discountCountry: checkAndFindPassportCountry(passport), productType: productTypeEnum.GLOVES });

        let glovePerPrice = productInInventoryModel[country].gloves.price;
        const checkForLimitOverForGloves = checkAndCalculateItemOverlimitFromOneInventory({
            currentCountryDetails: {
                country,
                perPiecePrice: glovePerPrice
            },
            minPriceFromOtherCountry: minGlovesPriceFromOtherCountry,
            productType: productTypeEnum.GLOVES,
            quantity: gloves
        })
        if (checkForLimitOverForGloves.isApplied) {
            totalGlovesPrice = checkForLimitOverForGloves.totalPrice;
            returnModel.gloves[countryCodeEnum.UK] = checkForLimitOverForGloves.itemsRemaining[countryCodeEnum.UK];
            returnModel.gloves[countryCodeEnum.GERMANY] = checkForLimitOverForGloves.itemsRemaining[countryCodeEnum.GERMANY];
        } else {
            totalGlovesPrice = (Math.floor(gloves / 10)) * Math.min(minGlovesPriceFromOtherCountry.perPiecePriceAfterShipment, glovePerPrice) * 10
                +
                Math.min(gloves % 10 * glovePerPrice, (gloves % 10) * minGlovesPriceFromOtherCountry.perPiecePrice + minGlovesPriceFromOtherCountry.shipmentPricePer10Unit);
            if (minGlovesPriceFromOtherCountry.perPiecePriceAfterShipment < glovePerPrice) {
                returnModel.gloves[minGlovesPriceFromOtherCountry.countryName] -= Math.floor(gloves / 10) * 10;
            } else {
                returnModel.gloves[country] -= Math.floor(gloves / 10) * 10;
            }
            if (minGlovesPriceFromOtherCountry.shipmentPricePer10Unit + (gloves % 10) * minGlovesPriceFromOtherCountry.perPiecePrice < gloves % 10 * glovePerPrice) {
                returnModel.gloves[minGlovesPriceFromOtherCountry.countryName] -= gloves % 10;
            } else returnModel.gloves[country] -= gloves % 10;
        }

        const maskPerPrice = productInInventoryModel[country].mask.price
        const minMaskPriceFromOtherCountry = checkMinProductPrice({ ignoreCountry: country, discountCountry: checkAndFindPassportCountry(passport), productType: productTypeEnum.MASK });

        const checkForLimitOverForMask = checkAndCalculateItemOverlimitFromOneInventory({
            currentCountryDetails: {
                country,
                perPiecePrice: maskPerPrice
            },
            minPriceFromOtherCountry: minMaskPriceFromOtherCountry,
            productType: productTypeEnum.MASK,
            quantity: mask
        })
        if (checkForLimitOverForMask.isApplied) {
            totalMaskPrice = checkForLimitOverForMask.totalPrice;
            returnModel.mask[countryCodeEnum.UK] = checkForLimitOverForMask.itemsRemaining[countryCodeEnum.UK];
            returnModel.mask[countryCodeEnum.GERMANY] = checkForLimitOverForMask.itemsRemaining[countryCodeEnum.GERMANY];
        } else {
            totalMaskPrice = (Math.floor(mask / 10)) * Math.min(minMaskPriceFromOtherCountry.perPiecePriceAfterShipment, maskPerPrice) * 10 +
                Math.min(mask % 10 * maskPerPrice, (mask % 10) * minGlovesPriceFromOtherCountry.perPiecePrice + minMaskPriceFromOtherCountry.shipmentPricePer10Unit);
            if (minMaskPriceFromOtherCountry.perPiecePriceAfterShipment < maskPerPrice) {
                returnModel.mask[minMaskPriceFromOtherCountry.countryName] -= Math.floor(mask / 10) * 10;
            } else {
                returnModel.mask[country] -= Math.floor(mask / 10) * 10;
            }
            if (minMaskPriceFromOtherCountry.shipmentPricePer10Unit + (mask % 10) * minMaskPriceFromOtherCountry.perPiecePrice < mask % 10 * maskPerPrice) {
                returnModel.mask[minMaskPriceFromOtherCountry.countryName] -= mask % 10;
            } else returnModel.mask[country] -= mask % 10;

        }
        returnModel.totalPrice = totalMaskPrice + totalGlovesPrice;
    }
    return returnModel;

}
function formattingOutput(model){
    let str = '';
    str += model.totalPrice + ':' + model.mask[countryCodeEnum.UK]  + ':' + model.mask[countryCodeEnum.GERMANY] + ':' + model.gloves[countryCodeEnum.UK]  + ':' + model.gloves[countryCodeEnum.GERMANY];
    if(model.message) str= model.message +':'+ model.mask[countryCodeEnum.UK]  + ':' + model.mask[countryCodeEnum.GERMANY] + ':' + model.gloves[countryCodeEnum.UK]  + ':' + model.gloves[countryCodeEnum.GERMANY];
    return str;
}   
function main() {

    const inputs = [
        'UK:B123AB1234567:Gloves:20:Mask:10',
        'Germany:B123AB1234567:Gloves:22:Mask:10',
        'UK:AAB123456789:Gloves:125:Mask:70',
        'Germany:AAB123456789:Mask:50:Gloves:25',
        'UK:Gloves:50:Mask:150',
        'UK:Gloves:250:Mask: 150'
    ]

    inputs.forEach(input => {
        let inputJSON = formattingInput(input);
        console.log(`Output `, formattingOutput(minimizeSalePrice(inputJSON)));
    });

}

main();