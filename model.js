const productInInventoryModel = {
    UK: {
        gloves: {
            price: 100,
            quantity: 100
        },
        mask: {
            price: 65,
            quantity: 100
        }
    },
    Germany: {
        gloves: {
            price: 150,
            quantity: 50
        },
        mask: {
            price: 100,
            quantity: 100
        }
    }
}
module.exports =  {
    productInInventoryModel
}